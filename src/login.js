import React, { Component } from "react";
import fire from "./config/fire";
import "./login.css";

class Login extends Component {
    //setting up input state for email and password
    state = {
        email: "",
        password: ""
    };
    //function for login
    login = e => {
        e.preventDefault();
        fire
            .auth()
            .signInWithEmailAndPassword(this.state.email, this.state.password)
            .catch(error => {
                console.log("Error Found", error);
            });
    };
    //function for signup
    signup = e => {
        e.preventDefault();
        fire
            .auth()
            .createUserWithEmailAndPassword(this.state.email, this.state.password)
            .catch(error => {
                console.log("Error Found", error);
            });
    };
    //function to handle input fields
    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    render() {
        return (
            <div className="main">
                <header>
                    <h1>File Collaborator</h1>
                </header>
                <div className="left">
                    <h2>Work Together</h2>
                    <p>This platform allows you to work on<br /> project together from different locations.<br />You can add,comment and edit the files<br /> all at one place.</p>
                </div>
                <div className="right">
                    <form id="Login-Form">
                        <img src="http://www.uptimecode.com/images/icons/user.png" id="person" alt="Login" />
                        <br />
                        <label>Email</label>
                        <br />
                        <input value={this.state.email} onChange={this.handleChange} className="info" type="email" name="email" placeholder="you@example.com" />
                        <br />
                        <label>Password<br /></label>
                        <input value={this.state.password} onChange={this.handleChange} className="info" type="password" name="password" placeholder="Enter Password" />
                        <p id="warn">Make sure that your password is atleast 6 characters long.</p>
                        <br />
                        <br />
                        <button type="submit" className="click" onClick={this.login}>Login</button>
                        <br />
                        <button className="click" onClick={this.signup}>Sign up</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default Login;
