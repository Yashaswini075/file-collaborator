import React, { Component } from 'react';
import { storage, fire } from '../config/fire';
import './fileupload.css';
import axios from 'axios';
// require( '@ckeditor/ckeditor5-build-decoupled-document' );
// import CKEditor from '../ckeditor/ckeditor';
import CKEditor from "react-ckeditor-component";

class FileUpload extends Component {
    state = {
        file: null,
        url: '',
        progress: 0,
        data: ''
    }
    handleChange = e => {
        if (e.target.files[0]) {
            const file = e.target.files[0];
            this.setState(() => ({ file }));
        }
    }
    logout = e => {
        fire.auth().signOut();
    }
    handleUpload = () => {
        const { file } = this.state;
        const uploadTask = storage.ref(`files/${file.name}`).put(file);
        uploadTask.on('state_changed',
            (snapshot) => {
                //progress function....
                const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                this.setState({ progress });
            },
            (error) => {
                //error function
                console.log(error);
            },
            () => {
                //complete function
                storage.ref('files').child(file.name).getDownloadURL().then(url => {
                    console.log(url);
                    this.setState({ url });
                })
            });

    }

    onChange = (evt) => {
        var newData = evt.editor.getData();
        this.setState({
            data: newData
        })
    }
    componentDidMount() {

        axios.get('/readFile')
            .then(response => {
                console.log(response);
                this.setState({
                    data: response.data
                })
            }
            )
            .catch(function (error) {
                console.log(error);
            });

        // axios({
        //     method: 'post',
        //     url: 'https://writer.zoho.com/v1/officeapi/document',
        //     data: {
        //         apikey: '04ab8721453b71f0bfeb16fd0b1c43ca',
        // document: 'file:///C:/Users/YML/Desktop/edited%20resume%20form.html',
        // permissions: {
        //     'document.export':true,
        //     'document.print':true,
        //     'document.edit':true,
        //     'review.changes.resolve':false,
        //     'review.comment':true,
        //     'collab.chat':true
        // }
        //     }
        // });
    }
    render() {
        // const style = {
        //     height: '100vh',
        //     display: 'flex',
        //     flexDirection: 'column',
        //     alignItems: 'center',
        //     justifyContent: 'center'
        // };
        return (
            <div className="style">
                <header>
                    <h1>File Collaborator</h1>
                </header>
                <progress value={this.state.progress} max="100" />
                <br />
                <input type="file" onChange={this.handleChange} />
                <button onClick={this.handleUpload}>Upload</button>
                <br />
                <img src={this.state.url || 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/File_alt_font_awesome.svg/512px-File_alt_font_awesome.svg.png'} alt="Uploaded files" height="50" width="50" />
                <object data={this.state.url} width="300" height="300">
                    <a id="display" href={this.state.url} >test.pdf</a>
                </object>
                {/* <textarea name="editor1" id="editor1" rows="10" cols="80" value={this.state.data}>

                </textarea> */}
                <CKEditor activeClass="editor" content={this.state.data} name="data" events={{ "change": this.onChange }} />
                <button id="end" onClick={this.logout}>Logout</button>
            </div>
        )
    }
}
export default FileUpload;