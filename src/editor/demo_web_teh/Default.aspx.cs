using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.ServiceModel;

using SubSystems.WebTeh;



namespace DmoTej
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class WebForm1 : System.Web.UI.Page
	{
    
		protected void Page_Load(object sender, System.EventArgs e)
		{

            // set position and other properties for the control
            WebTeh1.Left = 25;
            WebTeh1.Top = 40;
            WebTeh1.Width = 800;
            WebTeh1.Height = 410;

		
            // Put user code to initialize the page here
            if (!Page.IsPostBack) {
                //WebTej1.TejKey="...";   // set your product license key here, ensure that you have purchased a server license (not a desktop license)

                WebTeh1.Data="{\\rtf\\i Please type here...}";
                //WebTej1.Data="{\\rtf Test "+(new string((char)34,1))+"Test"+(new string((char)34,1))+ "Test}";
                RestoreFromServer.Enabled=false;   // enable when data saved for restoring
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

        }
		#endregion

        protected void SaveToServer_Click(object sender, System.EventArgs e)
        {
          Cache["RtfData"] = WebTeh1.Data;  // save the rtf data to show the 'restore' example
          RestoreFromServer.Enabled = true;
          
          // An example of saving a server file
 
           //if (FileName.Text.Length>0) {  // save data to disk file
           //    StreamWriter _rtfFile = new StreamWriter(FileName.Text);
           //    _rtfFile.Write( WebTeh1.Data);
           //   _rtfFile.Close();             
           //}
        }

        protected void RestoreFromServer_Click(object sender, System.EventArgs e)
        {
           WebTeh1.Data=(string)Cache["RtfData"];
           Cache["RtfData"]="";
           RestoreFromServer.Enabled=false;  // nothing more to restore
        }


        
	}
}
