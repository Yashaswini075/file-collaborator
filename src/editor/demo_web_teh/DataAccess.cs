﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
//using System.ServiceModel.Web;
using System.Text;
using System.IO;
using System.Web;


[ServiceContract(Namespace = "DataAccessService")]
[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
public class DataAccess
{
	// To use HTTP GET, add [WebGet] attribute. (Default ResponseFormat is WebMessageFormat.Json)
	// To create an operation that returns XML,
	//     add [WebGet(ResponseFormat=WebMessageFormat.Xml)],
	//     and include the following line in the operation body:
	//         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
	[OperationContract]
	public string GetRtf(string file)
	{
     string FileName=HttpRuntime.AppDomainAppPath+"\\"+file;

     if (FileName.Length == 0) return "Missing File Name";
     if (!File.Exists(FileName)) return "File not found: "+FileName;

     StreamReader InFile=null;
     string str = "";

     try {
       InFile = new StreamReader(FileName);
       str = InFile.ReadToEnd();
     }
     catch (Exception ex) {
       str=ex.ToString();    
     }
     InFile.Close();

     return str;
	}

  /// <summary>
  /// The file is saved in chunks by successively appending to the file.
  /// This is done to avoid the server error 500 (general server error)
  /// The FileAndRtf includes the file to save, the file offset, and the rtf data.  These three parameters
  /// are delimited by a '|' character
  /// </summary>
  /// <param name="file"></param>
  /// <returns></returns>
  [OperationContract]
  public bool SaveRtf(string FileAndRtf)
  {
    bool result = false;

    int DelimPos1 = FileAndRtf.IndexOf('|');  // look for the delimter after the file-name
    if (DelimPos1 <= 0) return false;

    string FileName = FileAndRtf.Substring(0, DelimPos1);   // file-name to save the rtf data
    if (FileName.Length == 0) return false;

    FileName = HttpRuntime.AppDomainAppPath + "\\" + FileName;

    int DelimPos2 = FileAndRtf.IndexOf('|', DelimPos1 + 1);  // look for the delimter after the file-offset
    if (DelimPos2 <= 0) return false;

    string sOffset = FileAndRtf.Substring(DelimPos1 + 1, DelimPos2 - DelimPos1 - 1);
    int offset = Convert.ToInt32(sOffset);
    if (offset < 0) return false;

    string rtf = FileAndRtf.Substring(DelimPos2 + 1);  // everything beyond the delimiter is the rtf to be saved

    if (offset==0 && File.Exists(FileName)) {  // delete the existing file
      if ((File.GetAttributes(FileName) & FileAttributes.ReadOnly) != 0) return false;     // read-only file
      try {           // 20130919
          File.Delete(FileName);
      }
      catch (Exception) {
          return false;
      }
    }

    StreamWriter oFile = null;

    try {
      bool append = (offset > 0);
      oFile = new StreamWriter(FileName, append, Encoding.ASCII);
      char[] crtf = rtf.ToCharArray();
      oFile.Write(crtf, 0, rtf.Length);
 
      result = true;
    }
    catch (Exception) {
    }
    oFile.Close();

    return result;
  }

	// Add more operations here and mark them with [OperationContract]
}
