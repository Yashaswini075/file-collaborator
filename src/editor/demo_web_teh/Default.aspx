<%@ Page language="c#" Inherits="DmoTej.WebForm1" CodeFile="Default.aspx.cs" %>
<%@ Register TagPrefix="cc1" Namespace="SubSystems.WebTeh" Assembly="WebTeh" %>
<%@ Register assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI" tagprefix="asp" %>
<%@ Register assembly="WebTeh" namespace="SubSystems.WebTeh" tagprefix="cc2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
  <head>
        <title>WebForm1</title>
<meta content="False" name="vs_snapToGrid"/>
<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
<meta content="C#" name="CODE_LANGUAGE"/>
<meta content="JavaScript" name="vs_defaultClientScript"/>
<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
        <style type="text/css">
          #Form1
          {
            height: 19px;
            z-index: 1;
            left: 10px;
            top: 15px;
            position: absolute;
            width: 1011px;
          }
        </style>
  </head>

<body>

   <!-- PLEASE NOTE:
        1. The control automatically generates a Javascript function to save the editor data before exiting.  
        This function is named using the format Save<ControlName>, example: SaveWebTeh1, SaveWebTej2

        The save function such as SaveWebTeh1() can be called from the submit button's click event to 
        save the updated WebTej1 data to be send to the server. You are free to device any other mechanism
        such as calling the SaveWebTeh1 function from the Preprocess event for command id ID_SAVE.
        
        In this example, the SaveWebTeh1 function is called from the click event (onclientclick) of the 
        SaveToServer button.
        
       2.  All editor constant such as ID_PASTE is accessed using the tc prefix, example:
          tej.TerCommand(tc.ID_PASTE)

   -->

   <!--TE Initialization Code  -->
<script type="text/javascript">
     <!--
    var tej;   // we maintain a global variable tej so other methods can use them

    function ControlCreated(WebTej)  // this gets called when the edit control is created and displayed
    {
         tej = WebTej;
          console.log("Page is loaded!");
          
          // Example of adding or removing toolbar icons
          //tej.TerAddToolbarIcon(0,0,tc.ID_SUPSCR_ON,"images/icon1.bmp","superscript"); 
          //tej.TerAddToolbarIcon(0,0,tc.ID_SUBSCR_ON,"images/icon2.bmp","subscript");
          //tej.TerHideToolbarIcon(tc.TLB_HELP,true);  // remove this icon

          //tej.TerRecreateToolbar(true);
        
          // Example of disabling a speedkey
          //tej.TerEnableSpeedKey(tc.ID_CHAR_STYLE,0);  // disable the original Alt+1 keycommand

     } 
 
     //-->
</script>

<form id="Form1" method="post" runat="server"><asp:label id="Label1" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 8px; height: 12px;" runat="server" Width="262px" ForeColor="RoyalBlue" Font-Size="Large" Font-Italic="True" Font-Bold="True">TE Edit Control Demo</asp:label><asp:button id="SaveToServer" onclientclick="SaveWebTeh1();" style="Z-INDEX: 102; LEFT: 32px; POSITION: absolute; TOP: 496px"  runat="server" Width="176px" Height="24px" Text="Save Text To Server" onclick="SaveToServer_Click"></asp:button>

<cc2:WebTeh ID="WebTeh1" runat="server" width="400" tabindex="0" height="400" style="Z-INDEX: 103; POSITION: absolute; LEFT: 15px; TOP: 100px;" />

<asp:label id="Label2" 
  style="Z-INDEX: 103; LEFT: 32px; POSITION: absolute; TOP: 472px" runat="server" 
  Width="185px" Height="13px" ForeColor="RoyalBlue" Font-Size="Medium" 
  Font-Italic="True" Font-Bold="True">Interaction with Server</asp:label><asp:button id="RestoreFromServer" style="Z-INDEX: 104; LEFT: 32px; POSITION: absolute; TOP: 520px" runat="server" Width="175px" Height="24px" Text="Restore Text From Server" onclick="RestoreFromServer_Click"></asp:button>
<asp:label id="lable3" 
  style="Z-INDEX: 105; LEFT: 240px; POSITION: absolute; TOP: 472px" 
  runat="server" Width="128px" Height="16px" ForeColor="RoyalBlue" 
  Font-Size="Medium" Font-Italic="True" Font-Bold="True">Table Functions</asp:label><input id="InsertTable" style="Z-INDEX: 106; LEFT: 232px; WIDTH: 136px; POSITION: absolute; TOP: 496px; HEIGHT: 24px" onclick="InsertTableClick();" type="button" value="Insert Table" name="InsertTable"/> 
<input style="Z-INDEX: 107; LEFT: 232px; WIDTH: 136px; POSITION: absolute; TOP: 528px; HEIGHT: 24px" onclick="InsertRowClick();" type="button" value="Insert Row" name="InsertRow" id="InsertRow"/> 
<input style="Z-INDEX: 108; LEFT: 232px; WIDTH: 136px; POSITION: absolute; TOP: 552px; HEIGHT: 24px" onclick="InsertColClick();" type="button" value="Insert Column" name="InsertCol" id="InsertCol"/> 
<input style="Z-INDEX: 109; LEFT: 232px; WIDTH: 136px; POSITION: absolute; TOP: 576px; HEIGHT: 24px" onclick="MergeCellsClick();" type="button" value="Merge Cells" name="MergeCells" id="MergeCells"/> 
<input style="Z-INDEX: 110; LEFT: 232px; WIDTH: 136px; POSITION: absolute; TOP: 600px; HEIGHT: 24px" onclick="SplitCellsClick();" type="button" value="Split Cells" name="SplitCells" id="SplitCells"/> 
<input style="Z-INDEX: 111; LEFT: 232px; WIDTH: 136px; POSITION: absolute; TOP: 624px; HEIGHT: 24px" onclick="DelCellsClick();" type="button" value="Delete Cells" name="DelCells" id="DelCells"/> 

<!--  not yet implemented
<input style="Z-INDEX: 112; LEFT: 32px; WIDTH: 176px; POSITION: absolute; TOP: 600px; HEIGHT: 24px" onclick="AutoSpellClick();" type="button" value="Auto Spell" name="AutoSpell" id="AutoSpell"/> 
-->

<input style="Z-INDEX: 115; LEFT: 32px; WIDTH: 176px; POSITION: absolute; TOP: 624px; HEIGHT: 24px" onclick="TestCodeClick();" type="button" value="Test code" name="TestCode" id="TestCode"/>
      <p>
        <asp:Label ID="LabelLoadFile" runat="server" 
          style="top: 497px; left: 394px; position: absolute; height: 19px; width: 339px; z-index: 111" 
          Text="Select a new file to load from Server (using AJAX) "></asp:Label>
        <asp:Label ID="LabelSaveFile" runat="server" 
           style="top: 527px; left: 394px; position: absolute; height: 19px; width: 339px; z-index: 111" 
           Text="Select a new Server file to save (using AJAX)"></asp:Label>
      </p>
      <asp:DropDownList ID="LoadFileList" runat="server" onchange="LoadFile(this.value);"
  style="z-index: 110; left: 743px; top: 493px; position: absolute; width: 120px;" >
        <%--<asp:ListItem Selected="True" Value="">Select a file</asp:ListItem>--%>
        <asp:ListItem>Demo1.rtf</asp:ListItem>
        <asp:ListItem>Demo2.rtf</asp:ListItem>
        <asp:ListItem>test.rtf</asp:ListItem>
</asp:DropDownList>

      <asp:DropDownList ID="DropDownList2" runat="server" onchange="SaveFile(this.value);"
  style="z-index: 110; left: 743px; top: 525px; position: absolute; width: 120px;" >
        <asp:ListItem Selected="True" Value="">Select a file</asp:ListItem>
        <asp:ListItem>Demo1.rtf</asp:ListItem>
        <asp:ListItem>Demo2.rtf</asp:ListItem>
        <asp:ListItem>test.rtf</asp:ListItem>
</asp:DropDownList>

<p>
    &nbsp;</p>
<asp:textbox id="FileName" style="Z-INDEX: 114; LEFT: 392px; POSITION: absolute; TOP: 624px" runat="server" Width="264px" Height="26px"></asp:textbox>


<script type="text/javascript">
         <!--

         //////////////////////////////////////////////////////////////////////////// 
         // Ter menu button activation -->
         //////////////////////////////////////////////////////////////////////////// 

         function InsertTableClick() {
            tej.TerCommand(tc.ID_TABLE_INSERT);
         }
            
         function InsertRowClick()   {
            tej.TerCommand(tc.ID_TABLE_INSERT_ROW);
         }
            
         function InsertColClick()   {
            tej.TerCommand(tc.ID_TABLE_INSERT_COL);
         }
         function MergeCellsClick()  {
            tej.TerCommand(tc.ID_TABLE_MERGE_CELLS);
         }
         function SplitCellsClick()  {
            tej.TerCommand(tc.ID_TABLE_SPLIT_CELL);
         }
         function DelCellsClick()    {
            tej.TerCommand(tc.ID_TABLE_DEL_CELLS);
         }

         function AutoSpellClick()   {
            tej.TerCommand(tc.ID_AUTO_SPELL);
         }  // for future use
               
         function TestCodeClick()    {
             // Toggle editing of header/footer
             //tej.TerCommand(tc.ID_EDIT_HDR_FTR);
         }
                 
         //////////////////////////////////////////////////////////////////////////// 
         // Ter menu button enable/disable function
         //////////////////////////////////////////////////////////////////////////////
         function UpdateToolbar() 
         {
             document.getElementById("InsertTable").disabled=!tej.TerMenuEnable2(tc.ID_TABLE_INSERT);   
             document.getElementById("InsertRow").disabled=!tej.TerMenuEnable2(tc.ID_TABLE_INSERT_ROW);
             document.getElementById("InsertCol").disabled=!tej.TerMenuEnable2(tc.ID_TABLE_INSERT_COL);
             document.getElementById("MergeCells").disabled=!tej.TerMenuEnable2(tc.ID_TABLE_MERGE_CELLS);
             document.getElementById("SplitCells").disabled=!tej.TerMenuEnable2(tc.ID_TABLE_SPLIT_CELL);
             document.getElementById("DelCells").disabled=!tej.TerMenuEnable2(tc.ID_TABLE_DEL_CELLS);

             document.getElementById("AutoSpell").disabled=!tej.TerMenuEnable2(tc.ID_AUTO_SPELL);
             
         }
  
          //////////////////////////////////////////////////////////////////////////// 
          // An example of using the Preprocess event 
          //////////////////////////////////////////////////////////////////////////// 
         function Preprocess(ActionType, ActionId) 
         {      
             //tej.PrintStr("action type: "+ActionType+" id: "+ActionId);
             
             if (ActionType == tc.ACTION_COMMAND && ActionId==tc.ID_SAVE) {  
                 
                 // your action here 
                 SaveWebTeh1();      // an optional method of saving the content of the editor.  The saved
                                     // content is available to the server application when the form is  
                                     // actually sumbitted.
                 
                 window.alert("Document saved!");    
                 
                 tej.TerIgnoreCommand();       // tell TE to ignore processing of this command
             }
         }

         ////////////////////////////////////////////////////////////////////////////
         // An ASP.NET example of loading a file from the server using AJAX
         ////////////////////////////////////////////////////////////////////////////
         function LoadFile(FileName) {
           var xmlhttp;
           if (FileName == "" || FileName == " ") return;

           var service = new DataAccessService.DataAccess(); // DataAccessService name space and DataAccess defined in the DataAccess.svc file
           service.GetRtf(FileName, OnLoadFileSuccess, null, null);  // GetRtf method defined in the DataAccess.svc file
         }

         function OnLoadFileSuccess(FileData) {
            tej.TerSetModify(false);  //  clear the dirty flag so the new file can be loaded
            tej.SetTerBuffer(FileData, "");
         }


         ////////////////////////////////////////////////////////////////////////////
         // An ASP.NET example of saving the content of the control to a server file
         ////////////////////////////////////////////////////////////////////////////
         var OutputFile = "";
         var rtf = "";
         var TotalBytes = 0;
         var BytesWritten = 0;
         var BufSize = 1000;   // write in small chunkcs as the server the server throws error 500 (general server error).
         var SegLen;      // length of the current segment being written

         function SaveFile(FileName) {
           var xmlhttp;
           if (FileName == "" || FileName == " ") return;

           if (BytesWritten < TotalBytes) {
             this.alert("A file save already in progress!");
             return;
           }

           var service = new DataAccessService.DataAccess(); // DataAccessService name space and DataAccess defined in the DataAccess.svc file

           tej.TerSetModify(true);  // set the dirty bit to get the updated content
           rtf = tej.GetTerBuffer();

           OutputFile = FileName;
           TotalBytes = rtf.length;
           BytesWritten = 0;      

           // pass the file name, file-offset, and the rtf data segment as one parameter using a delimiter separator
           param = FileName + "|0|";  // 0 indicates the starting offset of the file to write the data
           
           SegLen = TotalBytes;
           if (SegLen > BufSize) SegLen = BufSize;

           param += rtf.substring(0, SegLen);  // from / to (exclusive)

           service.SaveRtf(param, OnSaveFileSuccess, null, null);  // GetRtf method defined in the DataAccess.svc file
         }

         function OnSaveFileSuccess(result) {
           if (!result) {
             this.alert("Save error, data NOT saved!");
             return result;
           }

           BytesWritten += SegLen;   // increment the bytes written

           if (BytesWritten >= TotalBytes) {  // callback after writing the last segment
              this.alert("RTF Data saved");
              return result;
           }

           
           // write the next segment 
           // pass the file name, file-offset, and the rtf data segment as one parameter using a delimiter separator
           param = OutputFile + "|" + BytesWritten + "|";  

           SegLen = (TotalBytes-BytesWritten);
           if (SegLen > BufSize) SegLen = BufSize;

           param += rtf.substring(BytesWritten, BytesWritten + SegLen);  // from / to (exclusive)

           var service = new DataAccessService.DataAccess(); // DataAccessService name space and DataAccess defined in the DataAccess.svc file
           service.SaveRtf(param, OnSaveFileSuccess, null, null);  // GetRtf method defined in the DataAccess.svc file

         }

         ////////////////////////////////////////////////////////////////////////////
          // An ASP example of loading a file from the server using AJAX
          ////////////////////////////////////////////////////////////////////////////
         function LoadFileASP(FileName) 
         {
               var xmlhttp;
               if (FileName == "" || FileName==" ") return;

               tej = document.getElementById("WebTej1");  // short cut variable

               if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                 xmlhttp = new XMLHttpRequest();
               }
               else {// code for IE6, IE5
                 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
               }
               xmlhttp.onreadystatechange = function () {
                 if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                   tej.TerSetModify(false);  //  clear the dirty flag so the new file can be loaded
                   tej.SetTerBuffer(xmlhttp.responseText, "");
                 }
               }
               xmlhttp.open("POST", "DataAccess.asp?FileName=" + FileName, true);
               xmlhttp.send();
         }

     //-->

</script>
            
            <!-- //////////////////////////////////////////////////////////////////////////// -->


      </form>
    </body>
</html>
