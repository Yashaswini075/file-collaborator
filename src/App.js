import React, { Component } from "react";
import "./App.css";
import fire from "./config/fire";
import Login from "./login";
import FileUpload from "./components/fileupload.jsx";

class App extends Component {
  //Setup state
  state = {
    user: {},
    response: ''
  };

  //Executes after all the components are mounted
  componentDidMount() {
    this.authListener();
    // this.callApi()
    //   .then(res => this.setState({ response: res.express }))
    //   .catch(err => console.log(err));
  }

  // callApi = async () => {
  //   const response = await fetch('/api/hello');
  //   const body = await response.json();

  //   if (response.status !== 200) throw Error(body.message);

  //   return body;
  // };
  //Executes everytime a user sign-in or sign-out
  authListener = () => {
    fire.auth().onAuthStateChanged(user => {
      if (user) {
        this.setState({ user });
      } else {
        this.setState({ user: null });
      }
    });
  };

  render() {
    return (
      <div className="Layout">
        {this.state.user ? <FileUpload /> : <Login />}
        <p className="App-intro">{this.state.response}</p>
      </div>
      
    );
  }
}

export default App;
