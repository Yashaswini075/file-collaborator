import firebase from 'firebase';
import 'firebase/storage';

//web setup for firebase
var config = {
  apiKey: "AIzaSyBv1MHUpD31yYR4r51UG_CdCaE_K67MG8c",
  authDomain: "file-collaborator.firebaseapp.com",
  databaseURL: "https://file-collaborator.firebaseio.com",
  projectId: "file-collaborator",
  storageBucket: "file-collaborator.appspot.com",
  messagingSenderId: "305313362358"
};
const fire = firebase.initializeApp(config);
const storage = firebase.storage();
export {
  storage, fire, firebase as default
}


